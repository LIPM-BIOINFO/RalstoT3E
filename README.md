
# RalstoT3E source code

The source code is now available @ [https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/RalstoT3E](https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/RalstoT3E/)



# Publications

- <strong>Repertoire, unified nomenclature and evolution of the Type III effector gene set in the Ralstonia solanacearum species complex.</strong>
Peeters, Nemo ; Carrère, Sébastien ; Anisimova, Maria ; Plener, Laure ; Cazalé, Anne-Claire ; Genin, Stephane .  <i>BMC genomics. 2013; doi: 10.1186/1471-2164-14-859</i>

- <strong> Pangenomic type III effector database of the plant pathogenic Ralstonia spp. </strong>
Sabbagh, Cyrus Raja Rubenstein ; Carrere, Sebastien ; Lonjon, Fabien ; Vailleau, Fabienne ; Macho, Alberto P ; Genin, Stephane ; Peeters, Nemo . 
<i>PeerJ. 2019; doi: 10.7717/peerj.7346</i>

# Contact
Sebastien.Carrere@inrae.fr

